#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdint>
#include "GinManager.h"

using namespace std;

vector<string> parse_tokens(string& s) {
    vector<string> v;
    auto pos = 0;
    while (pos != string::npos) {
        if (pos != 0) pos += 1;
        auto next = s.find(" ", pos);
        v.push_back(s.substr(pos, next - pos));
        pos = next;
    }
    return v;
}

int main() {
    int num_hands;
    cin >> num_hands;
    string temp;
    getline(cin, temp);

    int gin_count = 0;
    GinManager gin_manager;
    for (int test_case = 0; test_case < num_hands; ++test_case) {
        string input;
        getline(cin, input);
        
        vector<string> tokens = parse_tokens(input);
        vector<int> cards;
        for (string s : tokens) {
            cards.push_back(GinManager::string_to_card(s));
        }
        if (gin_manager.IsGin(cards)) {
            ++gin_count;
        }
    }
    cout << gin_count << endl;
    return 0;
}
