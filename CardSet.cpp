#include "CardSet.h"

using namespace std;

bool CardSet::HasDuplicates() {
    for (int i = 1; i <= 52; ++i) {
        if (card_counts[i] > 1) return true;
    }
    return false;
}

void CardSet::Insert(const int card) {
    card_counts[card]++;
}

void CardSet::Insert(const vector<int>& cards) {
    for (int c : cards) {
        card_counts[c]++;
    }
}

bool CardSet::operator==(const CardSet& other) const {
    for (int i = 1; i <= 52; ++i) {
        if (other.card_counts[i] != card_counts[i]) return false;
    }
    return true;
}

