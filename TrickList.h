#pragma once
#include "CardSet.h"
#include <vector>

class TrickList {
public:
    TrickList() {}
    ~TrickList() {}

    void insert(const std::vector<int>& trick);
    void clear();

    //There are only 3 kinds of tricks
    std::vector<std::vector<int>> fives;
    std::vector<std::vector<int>> fours;
    std::vector<std::vector<int>> threes;
};
