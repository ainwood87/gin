#include "TrickList.h"

using namespace std; 

void TrickList::insert(const vector<int>& trick) {
    if (trick.size() == 3) {
        threes.push_back(trick);
    } else if (trick.size() == 4) {
        fours.push_back(trick);
    } else if (trick.size() == 5) {
        fives.push_back(trick);
    }
}

void TrickList::clear() {
    fives.clear();
    fours.clear();
    threes.clear();
}
