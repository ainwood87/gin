#pragma once
#include <vector>
#include <map>
#include <cstring>
class CardSet {
public:
    CardSet() : card_counts{} {}
    ~CardSet() {}
    void Insert(const int card);
    void Insert(const std::vector<int>& cards);
    bool HasDuplicates();
    bool operator==(const CardSet& other) const;

    int card_counts[53];
};
namespace std {
    template <>
    struct hash<CardSet>
    {
        //and cs with 10 or fewer cards
        size_t operator()(const CardSet& cs) const {
            unsigned long long h = 0;
            int num = 0;
            for (unsigned long long i = 0; i < 52; ++i) {
                int count = cs.card_counts[1 + i];
                while (count) {
                    h = h | ( (i + 1) << 8 * num);
                    ++num;
                    --count;
                }
            }
            return hash<unsigned long long>{}(h);
        }
    };
}
