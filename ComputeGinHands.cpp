#include <iostream>
#include <unordered_set>
#include <vector>
#include <string>
#include "CardSet.h"
#include "GinManager.h"

using namespace std;

int main() {
    GinManager gin_manager;
    unordered_set<CardSet> s = gin_manager.ComputeGinHands();
    for (auto cs : s) {
        cout << gin_manager.cards_to_string(cs) << endl;
    }
    return 0;
}
